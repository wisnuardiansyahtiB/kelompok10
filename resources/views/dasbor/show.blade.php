@extends('dasbor.dashboard')
@section('title', 'Admin Dashboard')
@section('content')
<div class="card-header">
  <h3 class="card-title">Daftar Barang</h3>
</div>
<!-- /.card-header -->
<div class="card-body">
    <a href="/dasbor/post" class="btn btn-primary mb-3" >Tambah Barang</a>
    <table class="table table-bordered">
      <thead>
        <tr class="text-center">
          <th style="width: 10px">No</th>
          <th>Nama Barang</th>
          <th>Harga</th>
          <th>Stok</th>
          <th>Kategori</th>
          <th>Keterangan</th>
          <th>Gambar Produk</th>
          <th>Aksi</th>
        </tr>
      </thead>
      @foreach ($barang as $key => $value)
      <tbody>
        <tr>
            <td class="text-center">{{ $key + 1 }}</td>
            <td>{{ $value->nama }}</td>
            <td>
            Rp.{{ $value->harga }};
            </td>
            <td>{{ $value->stok }} pcs</td>
           
            <td>{{ $value->kategori->kategori}}</td>
          
            <td>{{ $value->keterangan }}</td>
            <td align="center"><img src="{{asset('img/'. $value->gambar_produk)}}" width="40px" ></td>
            <td class="text-center"><a href="/dasbor/edit/{{ $value->id }}" class="btn btn-sm btn-info">Edit</a> 
              <a href="/dasbor/destroy/{{$value->id}}" class="btn btn-sm btn-danger" onclick="return confirm('Yakin Menghapus Item ini?');" >Delete</a>
              <a href="dasbor/{{$value->id}}" class="btn btn-sm btn-primary">Lihat</a>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection