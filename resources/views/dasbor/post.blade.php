@extends('dasbor.dashboard')
@section('title', 'Tambah Postingan Barang')
@section('h1', 'Posting Barang')
@section('h3', 'Posting Barang')
@section('content')
<div class="card-header">
  <h3 class="card-title">Form Input Barang</h3>
</div>
        <div class="container">
          <form action="/dasbor" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group mt-4">
              <label for="nama">Nama Barang</label>
              <input type="text" class="form-control" id="nama" name="nama">
              @error('nama')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="harga">Harga Barang</label>
              <input type="harga" class="form-control" id="harga" name="harga">
              @error('harga')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="stok">Stok Barang</label>
              <input type="text" class="form-control" id="stok" name="stok">
              @error('stok')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="kategori_id">Kategori Barang</label>
              <select name="kategori_id" id="kategori_id" class="form-control">
                  <option value="">-</option>
                  @foreach ($kategori as $value)
                      <option value="{{$value->id}}"> {{$value->kategori}} </option>
                  @endforeach
              </select>
              @error('kategori_id')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
          </div>
            
            <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <textarea class="form-control" id="keterangan" rows="3" name="keterangan"></textarea>
                @error('keterangan')
                  <div class="alert alert-danger">
                      {{ $message }}
                  </div>
              @enderror
              </div>
              <div class="form-group">
                <label for="gambar_produk">Foto Produk</label>
                <input type="file" class="form-control" id="gambar_produk" name="gambar_produk">
                @error('gambar_produk')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
              </div>
            <input type="submit" value="Submit" />
          </form>
        </div>
@endsection