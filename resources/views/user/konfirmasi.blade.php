@extends('layouts.master')

@section('content')
<div class="fashion_section">
    {{-- <div id="main_slider" class="carousel slide" data-ride="carousel"> --}}
       <div class="carousel-inner">
          
         
             
         <div class="carousel-item active">
            <div class="container">
               <h1 class="fashion_taital">Konfirmasi Pembelian Barang</h1>
               <div class="fashion_section_2">
                  <div class="row">
                     <div class="col-lg-4 col-sm-4">
                        <div class="box_main">
                           <h4 class="shirt_text"> {{ $produk->kategori->kategori}} </h4>
                           <div class="tshirt_img"><img src="{{asset('img/'.$produk->gambar_produk)}}"></div>
                           <p class="price_text">Price  <span style="color: #262626;">Rp. {{$produk->harga}} ,-</span></p>
                           <div class="btn_main">
                            <div class="buy_bt"><a href="#"></a></div>
                            <div class="seemore_bt"><a href="#"></a></div>
                         </div>
                        </div>
                        
                     </div>
                     <div class="col-lg-8 col-sm-8">
                        <div class="box_main">
                           {{-- <h1 class="text" >{{$produk->nama}}</h1><br>
                           <p class="price_text">Price  <span style="color: #262626;"></span></p>
                           <div class="text">{{$produk->keterangan}}</div><br>
                           <div class="btn_main">
                            <div class="buy_bt"><a href="/dasbor">Beli Sekarang</a></div>
                            <div class="seemore_bt"><a href="#">Masukkan Keranjang  </a></div> --}}
                            <form action="/pesanan" method="POST" enctype="multipart/form-data">
                                @csrf
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Barang</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Harga</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th scope="row">1</th>
                                    <td>{{$produk->nama}}</td>
                                    <td>{{$produk->kategori->kategori}}</td>
                                    <td>Rp. {{$produk->harga}} ,-</td>
                                  </tr>
                                  <tr>
                                    
                                    <td colspan="3" class="text-right">Total Pembayaran</td>
                                    <td>Rp. {{$produk->harga}} ,-</td>
                                  </tr>
                                </tbody>
                              </table>
                              <button type="submit" class="btn btn-primary">Checkout</button>
                            </form>
                         </div>
                        </div>
                     </div>
                    
                     
                  </div>
               </div>
            </div>
         </div>
      
    </div>
</div>



@endsection