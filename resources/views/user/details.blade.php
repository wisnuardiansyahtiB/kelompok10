@extends('layouts.master')

@section('content')
<div class="fashion_section">
    {{-- <div id="main_slider" class="carousel slide" data-ride="carousel"> --}}
       <div class="carousel-inner">
          
         
             
         <div class="carousel-item active">
             @foreach ($produk as $item=>$key)
            <div class="container">
               <h1 class="fashion_taital">Detail Barang</h1>
               <div class="fashion_section_2">
                  <div class="row">
                     <div class="col-lg-4 col-sm-4">
                        <div class="box_main">
                           <h4 class="shirt_text"> {{ $key->kategori->kategori}} </h4>
                           <div class="tshirt_img"><img src="{{asset('img/'.$key->gambar_produk)}}"></div>
                           <p class="price_text">Price  <span style="color: #262626;">Rp. {{$key->harga}} ,-</span></p>
                           <div class="btn_main">
                            <div class="buy_bt"><a href="#"></a></div>
                            <div class="seemore_bt"><a href="#"></a></div>
                         </div>
                        </div>
                        
                     </div>
                     <div class="col-lg-8 col-sm-8">
                        <div class="box_main">
                           <h1 class="text" >{{$key->nama}}</h1><br>
                           {{-- <p class="price_text">Price  <span style="color: #262626;"></span></p> --}}
                           <div class="text">{{$key->keterangan}}</div><br>
                           <div class="btn_main">
                            <div class="buy_bt"><a href="pesanan/{{$key->id}}">Pesan Sekarang</a></div>
                            <div class="seemore_bt"><a href="#">Masukkan Keranjang  </a></div>
                         </div>
                        </div>
                     </div>
                    
                     
                  </div>
               </div>
            </div>
         </div>
      
    </div>
</div>

         @endforeach

@endsection