@extends('layouts.master')
@section('content')
    
<div class="fashion_section">
    <div id="main_slider" class="carousel slide" data-ride="carousel">
       <div class="carousel-inner">
          
         
          <div class="carousel-item active">
             <div class="container">
                <h1 class="fashion_taital">Man & Woman Fashion</h1>
                <div class="fashion_section_2">
                   <div class="row">

                      @foreach ($produk as $value)
                      <div class="col-lg-4 col-sm-4">
                         <div class="box_main">
                            <h4 class="shirt_text"> {{$value->nama}} </h4>
                            <p class="price_text">Price  <span style="color: #262626;"> {{$value->harga}}</span></p>
                            <div class="tshirt_img"><img src="{{asset('img/'.$value->gambar_produk)}}"></div>
                            <div class="btn_main">
                               <div class="buy_bt"><a href="pesanan/{{$value->id}}">Pesan Sekarang</a></div>
                               <div class="seemore_bt"><a href="dasbor/{{$value->id}}">Lihat Details</a></div>
                            </div>
                         </div>
                      </div>
                      @endforeach
                      
                      
                   </div>
                </div>
             </div>
          </div>
      
          
         
       
 </div>
 
@endsection