@extends('dasbor.dashboard')
@section('content')
<a href="/kategori/create" class="btn btn-primary mb-3 mt-4" >Tambah Data</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Kategori Barang</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->kategori}}</td>
                <td>
                    <form action="/kategori/{{$value->id}}" method="POST">
                        <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete" onclick="return confirm('Yakin Menghapus Kategori {{$value->kategori}}?');" >
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>Data Masih Kosong!</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection