@extends('dasbor.dashboard')
@section('content')
<div>
    <form action="/kategori" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="kategori">Kategori Barang</label>
            <input type="text" class="form-control" name="kategori" id="kategori" placeholder="Masukkan Nama Kategori Barang">
            @error('kategori')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection