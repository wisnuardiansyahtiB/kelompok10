<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['kategori'];


    public function barang(){
        return $this->hasMany('App\Barang');
    }
    public function pesanan_detail(){
        return $this->hasMany('App\PesananDetail','kategori_id','id');
    }
}
