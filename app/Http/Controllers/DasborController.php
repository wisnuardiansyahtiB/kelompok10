<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Barang;
use App\kategori;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class DasborController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('details');
    }

    public function index()
    {
        $kategori = kategori::all();
        return view ('dasbor.post', compact('kategori'));
    }
    public function post(Request $request)
    {
        // $barang = DB::table('barang')->insert([
        //     "nama" => $request["nama"],
        //     "harga" => $request["harga"],
        //     "stok" => $request["stok"],
        //     "keterangan" => $request["keterangan"]
        //     ]);
        // return redirect('/dasbor');

        $this->validate($request,[
            'nama' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'keterangan' => 'required',
            'kategori_id' => 'required',
            'gambar_produk' => 'required|mimes:jpg,bmp,png,jpeg'
            ]);
            
            $gambar = $request->gambar_produk;
            $name_img = time(). ' - ' . $gambar->getClientOriginalName();

            Barang::create([
                'nama' => $request->nama,
                'harga' => $request->harga,
                'stok' => $request->stok,
                'keterangan' => $request->keterangan,
                'kategori_id' => $request->kategori_id,
                'gambar_produk' => $name_img
            
        ]);
        
        $gambar->move('img', $name_img);
        Alert::success('Berhasil', 'Berhasil Menambahkan Data Baru!');
        return redirect('/dasbor')->with('success', 'Anda berhasil menambahkan file baru');
    }
    public function dasbor(){
        $barang = Barang::all();
        $kategori = kategori::all();
        return view('dasbor.show', compact('barang', 'kategori'));
    }
    public function posting(){
        return view('dasbor.post');
    }
    public function edit($id){
        $barang = Barang::all()->where('id', $id)->first();
        return view('dasbor.edit',['barang' => $barang]);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'keterangan' => 'required'
        ]);

        $query = Barang::find($id)
            ->update([
                'nama' => $request["nama"],
                'harga' => $request["harga"],
                'stok' => $request["stok"],
                'keterangan' => $request["keterangan"]
            ]);
        return redirect('/dasbor');
    }
    public function destroy($id) 
    {
    // menghapus data berdasarkan id yang dipilih
    Barang::find($id) -> delete();
    // Alihkan ke halaman 
    
    Alert::warning('Berhasil', 'Anda Telah menghapus Barang ini');
    return redirect('/dasbor');
    }

    public function show($id)
    {
        $produk = Barang::all()->where('id', $id);
        $kategori = kategori::all();
        return view ('user.details', compact('produk', 'kategori'));
    }
}
