<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\kategori;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $produk = Barang::all();
        $kategori = kategori::all();
        return view ('index', compact('produk', 'kategori')); 
       
    }

}
