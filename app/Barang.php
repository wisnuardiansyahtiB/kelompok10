<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    protected $table = 'barang';
    protected $fillable = ['nama','harga','stok','keterangan','kategori_id','gambar_produk'];

    public function kategori(){
        return $this->belongsTo('App\kategori');
    }
    public function pesanan_detail(){
        return $this->hasMany('App\PesananDetail','barang_id','id');
    }
}
